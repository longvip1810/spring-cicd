package entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * @author longnguyen on 25/09/2023
 * @product IntelliJ IDEA
 * @project spring-cicd
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Product {

    private Long id;

    private String name;

    private LocalDate manuDate;

    private BigDecimal price;
}
