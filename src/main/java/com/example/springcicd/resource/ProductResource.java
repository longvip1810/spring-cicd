package com.example.springcicd.resource;

import entity.Product;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

/**
 * @author longnguyen on 25/09/2023
 * @product IntelliJ IDEA
 * @project spring-cicd
 */
@RestController
@RequestMapping("/api/products")
public class ProductResource {

    private List<Product> products;

    public ProductResource() {
        this.products = List.of(
                new Product(1L, "Product 1", LocalDate.of(2023,9, 20), BigDecimal.valueOf(1199.2)),
                new Product(2L, "Product 2", LocalDate.of(2022,9, 12), BigDecimal.valueOf(199.2)),
                new Product(3L, "Product 3", LocalDate.of(2023,7, 19), BigDecimal.valueOf(1422.2)),
                new Product(4L, "Product 4", LocalDate.of(2022,6, 16), BigDecimal.valueOf(999.9)),
                new Product(5L, "Product 5", LocalDate.of(2023,5, 3), BigDecimal.valueOf(550))
        );

    }

    @GetMapping
    public List<Product> getProducts(){
        return products;
    }


}
